<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestCases extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'name',
        'case_pos',
        'module_id',
        'sub_module_id',
        'ticket',
        'summary',
        'description',
        'attachment',
    ];
    protected $table = "test_case";
}
