<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubModules extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'name',
        'sub_pos',
        'module_id',
        'sub_module_id',
    ];
    protected $table = "sub_module";
    public function module()
    {
        return $this->belongsTo(Modules::class,'id','module_id');
    }

}
