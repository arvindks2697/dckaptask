<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modules extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'name',
        'pos'
    ];
    protected $table = "module";
    public function sub_modules()
    {
        return $this->hasOne(SubModules::class,"module_id","id");
    }
}
