<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{TestCases,Modules};

class TestCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $testcases = TestCases::get();
        $modules = Modules::get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$testcases
            ]);
        }
        return view("test_cases_list",compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = time().$imagePath->getClientOriginalName();
            // $path = $request->file('file')->storeAs('uploads', $imageName, 'public');
            $path =  $request->file->move(public_path('uploads'), $imageName);
            TestCases::create([
                'module_id'=>$request->module,
                'ticket'=>$request->ticket,
                'description'=>$request->description,
                'summary'=>$request->summary,
                'attachment'=>$imageName,
            ]);
        }else{
            TestCases::create([
                'module_id'=>$request->module,
                'ticket'=>$request->ticket,
                'description'=>$request->description,
                'summary'=>$request->summary,
            ]);
        }   
        
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_test_cases(Request $request, $id)
    {
        $testcases = TestCases::where("module_id",$id)->get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$testcases
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {
        $testcases = TestCases::where('id',$id)->first();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$testcases
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = time().$imagePath->getClientOriginalName();
            $path =  $request->file->move(public_path('uploads'), $imageName);
            TestCases::where("id",$id)->update([
                'module_id'=>$request->module,
                'ticket'=>$request->ticket,
                'description'=>$request->description,
                'summary'=>$request->summary,
                'attachment'=>$imageName,
            ]);
        }else{
            TestCases::where("id",$id)->update([
                'module_id'=>$request->module,
                'ticket'=>$request->ticket,
                'description'=>$request->description,
                'summary'=>$request->summary,
            ]);
        }   
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        TestCases::where("id",$id)->delete();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }
}
