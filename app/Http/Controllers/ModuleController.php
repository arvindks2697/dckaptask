<?php

namespace App\Http\Controllers;
use App\Models\{Modules,SubModules,TestCases};
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modules = Modules::get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$modules
            ]);
        }
        return view('modules_list',compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function module_create(Request $request)
    {
        Modules::create([
            "name"=>$request->module,
            "pos"=>$request->pos,
        ]);
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }
    public function get_module(Request $request,$id)
    {
        $module = Modules::where('id',$id)->first();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$module
            ]);
        }
    }
    public function module_update(Request $request,$id)
    {
        $module = Modules::where('id',$id)->update([
            "name"=>$request->module,
            "pos"=>$request->pos,
        ]);
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }
    public function create(Request $request)
    {
        SubModules::create([
            "name"=>$request->sub_module,
            "module_id"=>$request->module,
        ]);
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }
    public function get_sub_module(Request $request,$id)
    {
        $module = SubModules::where('module_id',$id)->get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$module
            ]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Modules::where("id",$id)->delete();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "message"=>"success"
            ]);
        }
    }
}
