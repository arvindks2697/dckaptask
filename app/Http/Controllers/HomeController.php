<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Modules,SubModules,TestCases};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $modules = Modules::get();
        if($request->ajax()){
            return response()->json([
                "code"=>200,
                "data"=>$modules
            ]);
        }
        return view('home',compact('modules'));
    }
}
