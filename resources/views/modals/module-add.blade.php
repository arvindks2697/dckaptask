<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <input type="hidden" class="module_route" value="">
          <form id="module" method="POST">
            @csrf
            <input type="hidden" class="module-url" value="{{ route("module.post.add") }}">
            <input type="text" name="module" class="module-select form-control my-1" placeholder="Enter the module name">
            <input type="number" name="pos" class="module-pos form-control my-1" placeholder="Enter the module position">
            <br>
            <button type="button" class="btn btn-primary add_module">Save</button>
            <button type="button" class="btn btn-primary add_module" data-dismiss="modal">Save and continue</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Understood</button>
        </div> --}}
      </div>
    </div>
</div>