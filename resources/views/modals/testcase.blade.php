<div class="modal fade" id="staticBackdrop2" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel2" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <input type="hidden" class="case-url" value="{{ route("test.case.add") }}">
        <form id="module" method="POST" enctype="multipart/form-data">
          @csrf
          <select name="module" class="module-case-select form-control my-4">
            <option value="" disabled selected>Choose an Option</option>
            @if(isset($modules))
              @foreach ($modules as $module)
                <option value="{{ $module->id }}">{{ $module->name }}</option>
              @endforeach
            @endif
          </select>
          <textarea type="text" name="summary" class="summary form-control my-4" placeholder="Enter the summary"></textarea>
          <input type="text" name="ticket" class="ticket form-control my-4" placeholder="Enter a ticket number">
          <input type="text" name="description" class="description form-control my-4" placeholder="Enter a description">
          <div class="custom-file my-2">
            <input type="file" class="custom-file-input" id="test-case-attachment" aria-describedby="inputGroupFileAddon01" name="file">
            <label class="custom-file-label" for="test-case-attachment">Choose a file to attach</label>
          </div>
          <div class="button-group mt-5 ">
            <button type="button" class="btn btn-primary add_case">Save</button>
            <button type="button" class="btn btn-primary add_case" data-dismiss="modal">Save and continue</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  
          </div>          
        </form>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Understood</button>
      </div> --}}
    </div>
  </div>
</div>