<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <input type="hidden" class="module_route" value="">
          <form id="module" method="POST">
            @csrf
            <select name="module" id="" class="module-select form-control my-1">
              <option value="" disabled selected>Choose an Option</option>
              {{-- <option value=""></option>
              <option value=""></option>
              <option value=""></option>
              <option value=""></option>
              <option value=""></option> --}}
              @if(isset($modules))
                @foreach ($modules as $module)
                  <option value="{{ $module->id }}">{{ $module->name }}</option>
                @endforeach
              @endif
            </select>
            <input type="text" name="sub_module" class='form-control sub_module my-1'>
            <button type="button" class="btn btn-primary add_module">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Understood</button>
        </div> --}}
      </div>
    </div>
</div>