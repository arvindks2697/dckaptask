@extends('layouts.default')
@section('css')
<style>
    body{
        background:#194fffda;
    }
    .card{
        border-radius:15px;
        box-shadow: 5px 5px 18px -1px rgba(0,0,0,0.75);
        -webkit-box-shadow: 5px 5px 18px -1px rgba(0,0,0,0.75);
        -moz-box-shadow: 5px 5px 18px -1px rgba(0,0,0,0.75);
        margin-top:35%;
    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                {{-- <div class="card-header">{{ __('Login') }}</div> --}}

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="text-center my-2">
                            <h2 class="mt-4 mb-2">Login into your Account</h2>
                            <h6 class="mt-2 mb-4">Don't Have an Account? <a href="{{ route('register') }}" class="text-primary">Sign up</a></h6>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email address">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your address">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                {{-- <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div> --}}
                                @if (Route::has('password.request'))
                                    <h6 class="float-right"><a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Recover Password?') }}
                                    </a></h6>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary w-100">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
