@extends('layouts.default')
@section('content')
<div class="container-fluid mt-3">
    <div class="row mb-4">
        <div class="col-6">
            <a href="{{ route('home') }}"class="btn btn-primary">Home</a>
            <a href="{{ route('module.list') }}"class="btn btn-primary">All Modules</a>
        </div>
        <div class="col-3"></div>
        <div class="col-3">
            <div class="bg-light d-flex justify-content-center">
                <div class="btn btn-transparent">Not Yet Approved</div>
                <div class="btn btn-transparent">Approved</div>
                <div class="btn btn-transparent">Rejected</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <a class="float-left"><i class="fas fa-chevron-left"></i>&nbsp;Previous Project</a>
        </div>
        <div class="col-6">
            <a class="float-right">Next Project&nbsp;<i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-primary text-light">
                    <div class="d-flex">
                        <div class="w-5"><i class="fas fa-arrow-left float-left mt-1"></i></div>
                        <div class="w-50 text-left">Module</div>
                        <div class="w-45 cursor-pointer add_toggle text-right" data-toggle="modal" data-target="#staticBackdrop2">+ Add</div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped w-100 test-case-table text-center">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Attachment</th>
                                <th>Ticket</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf('modals.testcase')
@endsection
@section("scripts")
<script>
    var table = $(".test-case-table").DataTable({
      "ajax":"{{ route('test.case.list') }}",
      "columns": [
            {   "data": "id" ,
                "render": function ( data, type, row, meta ) {
                    var template = `<i class="fas fa-arrows-alt mt-2" data-id="${data}"></i>`
                    return template;
                }
            },
            { 
                "data": "attachment",
                "render": function ( data, type, row, meta ) {
                    if(data !== null && data !== ""){
                        var template = `<a class="text-primary"><i class="fas fa-file"></i>&nbsp;${data}</a>`
                        return template;
                    }else{
                        return "No attachment"
                    }
                }
            },
            { 
                "data": "ticket",
                "render": function ( data, type, row, meta ) {
                    var content = "";
                    if(data !== null && data !== ""){
                        content = data;
                    }else{
                        content = "Not yet assigned"
                    }
                    var template = `<span class="badge badge-pill badge-primary">${content}</span>`
                    return template;
                }
            },
            { "data": "description" },
            { 
                "data": "id",
                "render": function ( data, type, row, meta ) {
                    var template = `<div class="d-flex justify-content-center">
                        <button type="button" class="btn btn-primary edit_toggle mr-2" data-toggle="modal" data-target="#staticBackdrop2"  data-id=${data} data-url="testcases/edit/${data}"><i class="fas fa-edit text-light"></i></button>
                        <button type="button" class="btn btn-primary delete_entry" data-id=${data} data-url="testcases/remove/${data}"><i class="fas fa-trash text-light"></i></button>
                    </div>`
                    return template;
                }
            },
        ],
        stateSave: true,
        rowReorder: {
            dataSrc: 'id'
        }
    });
    $(".add_case").on("click",function(){
        var form = $(this).parent().parent();
        var formdata =  new FormData(form[0]);
        if($(".module_select").val() == "" || $(".summary").val() == ""){
            alert("Please fill all the credentials");
            return false;
        }
        var url = $(".case-url").val();
        $.ajax({
            url: url,
            type:"POST",
            data:formdata,
            cache : false,
            processData:false,
            enctype: 'multipart/form-data',
            contentType: false,
            success: function(response){
                console.log(response);
                table.ajax.reload();
            }
        });
    })
    $(document).on("click",".edit_toggle",function(){
        var url = `testcases/post-edit/`+$(this).attr("data-id");
        $(".case-url").val(url);
        var getcurrentdataurl = $(this).attr("data-url");
        console.log("getcurrentdataurl",getcurrentdataurl)
        $.ajax({
            url: getcurrentdataurl,
            type:"GET",
            success: function(response){
                console.log(response,"response")
                var data= response.data;
                $(".module-case-select").val(data.module_id);
                $(".summary").val(data.summary);
                $(".ticket").val(data.ticket);
                $(".description").val(data.description);
                $(".custom-file-label").text(data.attachment);
            }
        });
    })
    $(document).on("click",".add_toggle",function(){
        var url = "{{ route('test.case.add') }}";
        console.log("url",url)
        $(".case-url").val(url);
        $(".module-case-select").val("");
        $(".summary").val("");
        $(".description").val("");
        $(".ticket").val("");
        $(".custom-file-label").text("Choose a file to attach");
    })
    $(document).on("click",".delete_entry",function(){
        var url = `testcases/remove/`+$(this).attr("data-id");
        var formdata = new FormData();
        console.log("csrf","{{ csrf_token() }}")
        formdata.append("token","{{ csrf_token() }}")
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type:"POST",
            cache : false,
            processData:false,
            enctype: 'multipart/form-data',
            contentType: false,
            success: function(response){
                console.log(response,"response")
                table.ajax.reload();
            }
        });
    })
</script>
@endsection