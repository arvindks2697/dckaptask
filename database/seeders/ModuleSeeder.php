<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        $data = [
            [
                'name' => "Blog",
                'pos' => 1,
            ],
            [
                'name' => "Footer Section",
                'pos' => 2,
            ],
            [
                'name' => "Pricing",
                'pos' => 3,
            ],
            [
                'name' => "Header Section",
                'pos' => 4,
            ],
            [
                'name' => "Contact",
                'pos' => 5,
            ],
            [
                'name' => "Home Page",
                'pos' => 6,
            ]
        ];
        foreach($data as $item){
            DB::table('module')->insert([
                'name' => $item['name'],
                'pos' => $item['pos'],
            ]);
        }
    }
}
