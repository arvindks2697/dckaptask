<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/getTestCases', [App\Http\Controllers\TestCaseController::class, 'index'])->name('test.case.list');
Route::post('/module/add', [App\Http\Controllers\ModuleController::class, 'create'])->name('module.add');
Route::prefix('modules')->name('module.')->group(function () {
    Route::get('/', [App\Http\Controllers\ModuleController::class, 'index'])->name('list');
    Route::get('/sub-modules/{id}', [App\Http\Controllers\ModuleController::class, 'get_sub_module'])->name('sub.list');
    Route::post('/post/add', [App\Http\Controllers\ModuleController::class, 'module_create'])->name('post.add');
    Route::get('/edit/{id}', [App\Http\Controllers\ModuleController::class, 'get_module'])->name('get.edit');
    Route::post('/post-edit/{id}', [App\Http\Controllers\ModuleController::class, 'module_update'])->name('post.edit');
    Route::post('/remove/{id}', [App\Http\Controllers\ModuleController::class, 'destroy'])->name('post.remove');
});
Route::prefix('testcases')->name('test.case.')->group(function () {
    Route::get('/', [App\Http\Controllers\TestCaseController::class, 'index'])->name('list');
    Route::get('/get/{id}', [App\Http\Controllers\TestCaseController::class, 'get_test_cases'])->name('test.get');
    Route::post('/add', [App\Http\Controllers\TestCaseController::class, 'create'])->name('add');
    Route::get('/edit/{id}', [App\Http\Controllers\TestCaseController::class, 'edit'])->name('get.edit');
    Route::post('/post-edit/{id}', [App\Http\Controllers\TestCaseController::class, 'update'])->name('post.edit');
    Route::post('/remove/{id}', [App\Http\Controllers\TestCaseController::class, 'destroy'])->name('post.remove');
});

